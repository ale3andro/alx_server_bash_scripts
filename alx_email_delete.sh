#!/bin/bash
# Deletes e-mails on inbox and Sent folders of the sxoleio.pw mail server
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi
clear
baseFolder='/home'
cd $baseFolder
for i in ls -d */; do
    if [[ $i == st* ]] || [[ $i == pc* ]]; then
        cd $baseFolder/$i
        if [ -d "Maildir/cur" ]; then
            echo $i"Maildir/cur EXISTS, deleting it."
            sudo rm -r Maildir/cur/
        fi
        if [ -d "Maildir/.Sent/cur" ]; then
            echo $i"Maildir/.Sent/cur EXISTS. deleting it."
            sudo rm -r Maildir/.Sent/cur/
        fi
    fi
done

echo "All the e-mails (if any), were deleted, have a nice day!"