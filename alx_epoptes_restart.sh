#!/bin/bash
# Kills epoptes client process (xtightvnc) on clients
# Then kills epoptes on server
if [ "$EUID" -eq 0 ]
  then echo "This script was not intented to be used with eleveted privileges (ie as root)"
  exit
fi
clear
if [ $# -eq 0 ]; then
    echo "This script takes at least 1 argument - The school number"
    echo "1 - 1o Dimotiko"
    echo "9 - 9o Dimotiko"
    echo "12 - DS Axou"
    exit
fi
schoolid="$1"
cd /home/ale3andro/Code-repos/alx_pussh
./alx_run_command2 $schoolid 12
sudo killall epoptes
epoptes &
