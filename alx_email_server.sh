#!/bin/bash
# Deletes e-mails on inbox and Sent folders of the sxoleio.pw mail server
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi
if [ $# -eq 0 ]; then
    echo "This script takes at least 1 argument - The server status"
    echo "start - dovecot & postfix start"
    echo "stop - dovecot & postfix stop"
    echo "disable - dovecot & postfix disable auto start at boot"
    echo "enable - dovecot & postfix enable auto start at boot"
    echo "status - dovecot & postfix status"
    exit
fi
if [ $1 == 'start' ]; then
    echo -n 'Starting Dovecot: '
    sudo service dovecot start &
    PID=$!
    i=1
    sp="/-\|"
    echo -n ' '
    while [ -d /proc/$PID ]
    do
        printf "\b${sp:i++%${#sp}:1}"
    done
    echo 
    echo -n 'Starting postfix: '
    sudo service postfix start &
    PID=$!
    i=1
    sp="/-\|"
    echo -n ' '
    while [ -d /proc/$PID ]
    do
        printf "\b${sp:i++%${#sp}:1}"
    done
    echo 
elif [ $1 == 'stop' ]; then 
    echo -n 'Stopping Dovecot: '
    sudo service dovecot stop &
    PID=$!
    i=1
    sp="/-\|"
    echo -n ' '
    while [ -d /proc/$PID ]
    do
        printf "\b${sp:i++%${#sp}:1}"
    done
    echo 
    echo -n 'Stopping postfix: '
    sudo service postfix stop &
    PID=$!
    i=1
    sp="/-\|"
    echo -n ' '
    while [ -d /proc/$PID ]
    do
        printf "\b${sp:i++%${#sp}:1}"
    done
    echo 
elif [ $1 == 'enable' ]; then 
    echo -n 'Enabling Dovecot: '
    sudo systemctl enable dovecot &
    PID=$!
    i=1
    sp="/-\|"
    echo -n ' '
    while [ -d /proc/$PID ]
    do
        printf "\b${sp:i++%${#sp}:1}"
    done
    echo 
    echo -n 'Enabling postfix: '
    sudo systemctl enable postfix &
    PID=$!
    i=1
    sp="/-\|"
    echo -n ' '
    while [ -d /proc/$PID ]
    do
        printf "\b${sp:i++%${#sp}:1}"
    done
    echo 
elif [ $1 == 'disable' ]; then 
    echo -n 'Disabling Dovecot: '
    sudo systemctl disable dovecot &
    PID=$!
    i=1
    sp="/-\|"
    echo -n ' '
    while [ -d /proc/$PID ]
    do
        printf "\b${sp:i++%${#sp}:1}"
    done
    echo 
    echo -n 'Disabling postfix: '
    sudo systemctl disable postfix &
    PID=$!
    i=1
    sp="/-\|"
    echo -n ' '
    while [ -d /proc/$PID ]
    do
        printf "\b${sp:i++%${#sp}:1}"
    done
    echo 
else
    echo Servers status
    dovecot=`ps -e | grep dovecot | wc -l`
    postfix=`sudo service postfix status | grep dead | wc -l`
    if [ $dovecot == '1' ]; then
        echo 'Dovecot server: Running'
    else
        echo 'Dovecot server: Stopped'
    fi
    if [ $postfix == '1' ]; then
        echo 'Postfix server: Stopped'
    else
        echo 'Postfix server: Running'
    fi
fi

exit



